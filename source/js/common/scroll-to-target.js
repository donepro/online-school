import animate from './animate';
import {quadEaseInOut} from './timing-functions';
const TECHNICAL_OFFSET = 2;

export default (target, offset) => {
  const offsetY = offset || 0;
  const startCoords = window.pageYOffset;
  const targetCoords = target.getBoundingClientRect().top - offsetY + TECHNICAL_OFFSET;

  const animateOptions = {
    duration: 500,
    timing: quadEaseInOut,
    draw(progress) {
      window.scrollTo(0, startCoords + progress * targetCoords);
    }
  };
  animate(animateOptions);
};
