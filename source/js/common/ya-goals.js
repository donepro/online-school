export default (data) => {
  const isYa = typeof window.yaCounter46916706 !== `undefined`;

  if (isYa) {
    window.yaCounter46916706.reachGoal(data);
  }
};
