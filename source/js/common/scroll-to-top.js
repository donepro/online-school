import animate from './animate';
import {quadEaseInOut} from './timing-functions';

export default () => {
  const distance = window.pageYOffset;
  const animateOptions = {
    duration: 500,
    timing: quadEaseInOut,
    draw: (progress) => window.scrollTo(0, distance - progress * distance)
  };
  animate(animateOptions);
};
