import yaGoals from './common/ya-goals';

const links = document.querySelectorAll(`[data-ya]`);

for (const link of links) {
  link.addEventListener(`click`, () => {
    yaGoals(link.dataset.ya);
  });
}
