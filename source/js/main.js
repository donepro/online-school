import 'wicg-focus-ring';

import '../blocks/advantages';
import '../blocks/teachers';
import '../blocks/guarantee';
import '../blocks/process';
import '../blocks/page-header';
import '../blocks/training-options';
import '../blocks/opened-subscription';
import '../blocks/teacher-card';
import '../blocks/program-card';
import '../blocks/field-text';
import '../blocks/materials';
import '../blocks/modal-side';
import '../blocks/modal';
import '../blocks/reviews';
import '../blocks/universities';
import '../blocks/video';

import './goals';
