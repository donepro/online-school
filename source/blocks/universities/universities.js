import slick from 'slick-carousel'; //eslint-disable-line
import $ from 'jquery';
const slider = document.querySelector(`.universities__list`);

$(slider).slick({
  accessibility: false,
  centerMode: true,
  appendDots: document.querySelector(`.universities__wrapper`),
  arrows: false,
  dots: true,
  dotsClass: `universities__dots`,
  infinite: true,
  mobileFirst: true,
  rows: 0,
  variableWidth: true,
  swipeToSlide: true
});
