import slick from 'slick-carousel'; //eslint-disable-line
import $ from 'jquery';
const slider = document.querySelector(`.process__list`);


function processWindowSize() {
  if ($(window).width() <= '767') { //eslint-disable-line
    $(slider).slick({
      accessibility: false,
      centerMode: true,
      appendDots: document.querySelector(`.process__list`),
      arrows: false,
      dots: true,
      dotsClass: `guarantee__dots`,
      infinite: false,
      mobileFirst: true,
      rows: 0,
      variableWidth: true,
      swipeToSlide: true,
      adaptiveHeight: true
    });
  }
}

$(window).on('load resize', processWindowSize); //eslint-disable-line
