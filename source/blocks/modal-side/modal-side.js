import Popup from '../../js/common/popup';
// import reviews from '../reviews/data';
import options from '../training-options/data';
// import successCards from '../advantages/people';

import {getTemplate as getReviewTemplate} from '../side-review/side-review';
import {getTemplate as getOptionTemplate} from '../side-card/side-card';
// import {getTemplate as getSuccessCardTemplate} from '../success-card/success-card';

const modal = document.querySelector(`.modal-side`);

const TRANSITION_TIME = 1100;
const page = document.querySelector(`.page`);

let modalInner;
// let modalLeftInner;
let modalLeft;

if (modal !== null) {
  modalInner = modal.querySelector(`.modal-side__inner`);
  // modalLeftInner = modal.querySelector(`.modal-side__left-inner`);
  modalLeft = modal.querySelector(`.modal-side__left`);
}

const map = {
  sixMonth: () => {
    return getOptionTemplate(options.find((it) => it.data === `sixMonth`));
  },
  intensive: () => {
    return getOptionTemplate(options.find((it) => it.data === `intensive`));
  },
  threeDays: () => {
    return getOptionTemplate(options.find((it) => it.data === `threeDays`));
  }
};

const onLeftAreaClicked = () => {
  myPopup.close();
};

const myPopup = new Popup(modal, {
  visibleClass: `modal-side--visible`,
  openButtons: document.querySelectorAll(`[data-modal-side]`),
  closeButtons: document.querySelectorAll(`.modal-side__close`),
  beforeClass: `modal-side--before`,
  afterClass: `modal-side--after`,
  transitionTime: TRANSITION_TIME,
  onPopupOpen(button) {
    page.classList.add(`page--no-scrolling`);
    modalLeft.addEventListener(`click`, onLeftAreaClicked);
    if (typeof map[button.dataset.modalSide] !== `undefined`) {
      // modalInner.innerHTML = map[button.dataset.modalSide]();
      modalInner.innerHTML = getOptionTemplate(JSON.parse(button.dataset.stringify));
      // let successSort = [...successCards].sort(() => Math.random() - 0.5).slice(0, 2);
      // modalLeftInner.innerHTML = getSuccessCardTemplate(successSort[0]) + getSuccessCardTemplate(successSort[1]);
    } else {
      // modalInner.innerHTML = getReviewTemplate(reviews[button.dataset.modalSide]);
      modalInner.innerHTML = getReviewTemplate(JSON.parse(button.dataset.stringify));
    }
  },
  onPopupClose() {
    modalLeft.removeEventListener(`click`, onLeftAreaClicked);
    setTimeout(() => {
      page.classList.remove(`page--no-scrolling`);
      modalInner.innerHTML = ``;
      // modalLeftInner.innerHTML = ``;
    }, TRANSITION_TIME);
  }
});

if (modal !== null) {
  myPopup.init();
}

export default myPopup;
