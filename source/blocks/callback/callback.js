import Validate from '../../js/common/validate';
import send from '../../js/common/send';
import Inputmask from "inputmask/dist/inputmask/inputmask.js";
import yaGoals from '../../js/common/ya-goals';

export default () => {
  const callback = document.querySelector(`.callback`);

  if (callback !== null) {

    const telFields = callback.querySelectorAll(`.field-text__input[type="tel"]`);

    if (telFields.length > 0) {
      const myInputMask = new Inputmask(`+7 999 999 99 99`, {showMaskOnHover: false});
      myInputMask.mask(telFields);
    }

    const form = callback.querySelector(`.callback__form`);
    const thanks = callback.querySelector(`.callback__thanks`);
    const thanksButton = callback.querySelector(`.callback__one-more`);

    const showThanks = () => {
      form.classList.add(`callback__form--hidden`);
      thanks.classList.add(`callback__thanks--visible`);
      thanksButton.addEventListener(`click`, onThanksButtonClicked);
    };

    const hideThanks = () => {
      form.classList.remove(`callback__form--hidden`);
      thanks.classList.remove(`callback__thanks--visible`);
      thanksButton.removeEventListener(`click`, onThanksButtonClicked);
    };

    const onThanksButtonClicked = (evt) => {
      evt.preventDefault();
      hideThanks();
    };

    const myValidate = new Validate(form);

    myValidate.init();

    form.addEventListener(`submit`, (evt) => {
      evt.preventDefault();
      if (myValidate.checkValidity().isValid) {
        yaGoals(`order14`);
        let data = new FormData(form);
        let template = `\
        <b>Имя: </b> {{name}} <br>
        <b>Телефон: </b> {{phone}}`;

        data.append(`template`, template);
        // data.append(`theme`, `New Request from Online School`);
        // data.append(`recipient`, `qa@donepro.ru`);
        // data.append(`sender`, `qa@donepro.ru`);
        // data.append(`replyTo`, `qa@donepro.ru`);

        send({
          url: form.action,
          method: form.method,
          data,
          onSuccess() {
            showThanks();
            form.reset();
          },
          onError() {
            showThanks();
          }
        });
      }
    });
  }
};
