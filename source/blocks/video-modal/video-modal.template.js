import YouTubePlayer from 'youtube-player';

export const template = `
<section class="video-modal">
  <h2 class="video-modal__title"><span class="video-modal__marked">Обращение</span> основателя школы</h2>
  <div class="video-modal__video">
    <div id="videoModal"></div>
  </div>
</section>`;

export const bind = () => {
  const player = new YouTubePlayer(`videoModal`, {
    playerVars: {
      'rel': 0
    }
  });

  player.loadVideoById(`q7Dz5_89tdQ`);
  player.playVideo();
};
