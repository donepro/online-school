import slick from 'slick-carousel'; //eslint-disable-line
import $ from 'jquery';
const slider = document.querySelector(`.teachers__list`);


function windowSize() {
  if ($(window).width() <= '1279') { //eslint-disable-line
    $(slider).slick({
      arrows: false,
      centerMode: true,
      infinite: false,
      initialSlide: 1,
      mobileFirst: true,
      rows: 0,
      variableWidth: true,
      focusOnSelect: true,
    });
  }
}

$(window).on('load resize', windowSize); //eslint-disable-line
