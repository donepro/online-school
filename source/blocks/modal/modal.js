import Popup from '../../js/common/popup';

// import callbackTemplate from '../callback/callback.template';
import callbackBind from '../callback/callback';
// import questionTemplate from '../question/question.template';
import questionBind from '../question/question';
import {template as videoTemplate, bind as videoBind} from '../video-modal/video-modal.template';
import getBlankTemplate from '../blank-modal/blank-modal.template';

const modal = document.querySelector(`.modal`);

if (modal !== null) {

  const TRANSITION_TIME = 300;

  const map = {
    callback() {
      let callbackForm = document.querySelector(`.modal-callback`);
      // modalInner.innerHTML = callbackTemplate;
      modalInner.innerHTML = callbackForm.innerHTML;
      callbackBind();
    },
    question() {
      let questionForm = document.querySelector(`.modal-question`);
      // modalInner.innerHTML = questionTemplate;
      modalInner.innerHTML = questionForm.innerHTML;
      questionBind();
    },
    video() {
      modalInner.innerHTML = videoTemplate;
      videoBind();
    },
    blank(button) {
      modalInner.innerHTML = getBlankTemplate(button.dataset.src);
    }
  };

  const modalInner = modal.querySelector(`.modal__inner`);
  const page = document.querySelector(`.page`);

  const onOutsideAreaClicked = (evt) => {
    if (!modalInner.contains(evt.target)) {
      evt.preventDefault();
      myPopup.close();
    }
  };

  const myPopup = new Popup(modal, {
    visibleClass: `modal--visible`,
    openButtons: document.querySelectorAll(`[data-modal]`),
    closeButtons: document.querySelectorAll(`.modal__close`),
    beforeClass: `modal--fade-in`,
    afterClass: `modal--fade-out`,
    transitionTime: TRANSITION_TIME,
    onPopupOpen(button) {
      page.classList.add(`page--no-scroll`);
      map[button.dataset.modal](button);
      modal.addEventListener(`click`, onOutsideAreaClicked);
    },
    onPopupClose() {
      modal.removeEventListener(`click`, onOutsideAreaClicked);
      setTimeout(() => {
        page.classList.remove(`page--no-scroll`);
        modalInner.innerHTML = ``;
      }, TRANSITION_TIME);
    }
  });

  myPopup.init();
}
