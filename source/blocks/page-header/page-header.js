const mobileMenuButton = document.querySelector('.page-header__mobile-menu'); //eslint-disable-line
const pageNav = document.querySelector('.page-nav'); //eslint-disable-line

if (pageNav) {
    mobileMenuButton.addEventListener('click', function () { //eslint-disable-line
    mobileMenuButton.classList.toggle('page-header__mobile-menu--opened'); //eslint-disable-line
    pageNav.classList.toggle('page-nav--open'); //eslint-disable-line
  });
}

