import YouTubePlayer from 'youtube-player';

const play = document.querySelector(`.reviews__video`);

if (play !== null) {
  play.addEventListener(`click`, (evt) => {
    evt.preventDefault();

    window.player1337 = new YouTubePlayer(`video-review`, {
      playerVars: {
        'rel': 0
      }
    });

    window.player1337.loadVideoById(`OrPWmwI5rPk`);
    window.player1337.on(`stateChange`, (event) => {
      if (event.data === 0) {
        window.player1337.destroy();
      }
    });
  });
}
