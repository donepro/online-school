const reviewsList = document.querySelector(`.reviews__list`);
const reviewsButton = document.querySelector(`.reviews__more`);
import slick from 'slick-carousel'; //eslint-disable-line
import $ from 'jquery';
const slider = document.querySelector(`.reviews__list`);

if (reviewsButton !== null) {
  reviewsButton.addEventListener(`click`, (evt) => {
    evt.preventDefault();
    reviewsList.classList.add(`reviews__list--all-visible`);
    reviewsButton.classList.add(`reviews__more--hidden`);
  });
}

function reviewsWindowSize() {
  if ($(window).width() <= '767') { //eslint-disable-line
    $(slider).slick({
      accessibility: false,
      centerMode: true,
      appendDots: document.querySelector(`.reviews__list`),
      arrows: false,
      dots: true,
      dotsClass: `reviews__dots`,
      infinite: false,
      mobileFirst: true,
      rows: 0,
      variableWidth: true,
      swipeToSlide: true
    });
  }
}

$(window).on('load resize', reviewsWindowSize); //eslint-disable-line
