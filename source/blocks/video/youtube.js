import YouTubePlayer from 'youtube-player';

const play = document.querySelector(`.video__container`);

if (play !== null) {
  play.addEventListener(`click`, (evt) => {
    evt.preventDefault();

    window.player1337 = new YouTubePlayer(`video-about`, {
      playerVars: {
        'rel': 0
      }
    });

    window.player1337.loadVideoById(`q7Dz5_89tdQ`);
    window.player1337.on(`stateChange`, (event) => {
      if (event.data === 0) {
        window.player1337.destroy();
      }
    });
  });
}
