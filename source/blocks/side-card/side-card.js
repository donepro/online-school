export const getTemplate = (item) => {
  return `\
<section class="side-card">
  <div class="side-card__wrapper">
    <h3 class="side-card__title">${item.title}</h3>
    <span class="side-card__period side-card__period--${item.period.mod}">${item.period.text}</span>
    <ul class="side-card__features">
    ${item.features.map((feature) => {
    return `\
      <li class="side-card__features-item">${feature}</li>`;
  }).join(``)}
    </ul>
    ${item.priceCaption ? `<p class="side-card__price-caption">${item.priceCaption}</p>` : ``}
    <div class="side-card__price-list">
    ${item.price.map((priceItem) => {
    return `\
      <div class="side-card__price-item">
        <div class="side-card__price-name">${priceItem.name}</div>
        <div class="side-card__price-value">
          <span class="side-card__marked-value">${priceItem.value} Р</span>/мес.
        </div>
      </div>`;
  }).join(``)}
    </div>
    ${item.priceSecondCaption ? `<p class="side-card__price-caption">${item.priceSecondCaption}</p>` : ``}
    ${item.priceSecond ? `\
    <div class="side-card__price-list">
    ${item.priceSecond.map((priceItem) => {
    return `\
      <div class="side-card__price-item">
        <div class="side-card__price-name">${priceItem.name}</div>
        <div class="side-card__price-value">
          <span class="side-card__marked-value">${priceItem.value} Р</span>/мес.
        </div>
      </div>`;
  }).join(``)}
    </div>` : ``}
    ${!item.isDelayed ? `\
    <a class="button button--gradient side-card__button" href="${item.href}"><span>Участвовать</span></a>` : ``}
  </div>
</section>`;
};
