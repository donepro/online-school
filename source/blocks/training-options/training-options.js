import $ from 'jquery';
const trainingOptions = document.querySelector('.training-options'); //eslint-disable-line


function trainingOptionWindowSizeMobileOS() {

  if (trainingOptions) {
    const trainingOptionOS = trainingOptions.querySelector('.training-option'); //eslint-disable-line
    const trainingOptionWrapperOS = trainingOptions.querySelector('.training-option__wrapper'); //eslint-disable-line
    const trainingOptionFeaturesOS = trainingOptions.querySelector('.training-option__features'); //eslint-disable-line

    if ($(window).width() <= '767') { //eslint-disable-line

      if (trainingOptionOS.classList.contains('training-option--no-features')) { //eslint-disable-line
        return;
      } else {
        trainingOptionOS.classList.add('training-option--no-features'); //eslint-disable-line
        trainingOptionWrapperOS.classList.remove('training-option__wrapper--tablet'); //eslint-disable-line
        trainingOptionFeaturesOS.classList.remove('training-option__features--tablet'); //eslint-disable-line
      }
    }
  }
}

function trainingOptionWindowSizeTabletOS() {

  if (trainingOptions) {
    const trainingOptionOS = trainingOptions.querySelector('.training-option'); //eslint-disable-line
    const trainingOptionWrapperOS = trainingOptions.querySelector('.training-option__wrapper'); //eslint-disable-line
    const trainingOptionFeaturesOS = trainingOptions.querySelector('.training-option__features'); //eslint-disable-line

    if ($(window).width() <= '1279' && $(window).width() >= '768') { //eslint-disable-line

      if (trainingOptionOS.classList.contains('training-option--no-features')) { //eslint-disable-line
        trainingOptionOS.classList.remove('training-option--no-features'); //eslint-disable-line
        trainingOptionWrapperOS.classList.add('training-option__wrapper--tablet'); //eslint-disable-line
        trainingOptionFeaturesOS.classList.add('training-option__features--tablet'); //eslint-disable-line
      }
    }
  }
}

function trainingOptionWindowSizeDesktopOS() {

  if (trainingOptions) {
    const trainingOptionOS = trainingOptions.querySelector('.training-option'); //eslint-disable-line
    const trainingOptionWrapperOS = trainingOptions.querySelector('.training-option__wrapper'); //eslint-disable-line
    const trainingOptionFeaturesOS = trainingOptions.querySelector('.training-option__features'); //eslint-disable-line

    if ($(window).width() >= '1280') { //eslint-disable-line

      if (trainingOptionOS.classList.contains('training-option--no-features')) { //eslint-disable-line
        return;
      } else {
        trainingOptionOS.classList.add('training-option--no-features'); //eslint-disable-line
        trainingOptionWrapperOS.classList.remove('training-option__wrapper--tablet'); //eslint-disable-line
        trainingOptionFeaturesOS.classList.remove('training-option__features--tablet'); //eslint-disable-line
      }
    }
  }
}

$(window).on('load resize', trainingOptionWindowSizeMobileOS); //eslint-disable-line
$(window).on('load resize', trainingOptionWindowSizeTabletOS); //eslint-disable-line
$(window).on('load resize', trainingOptionWindowSizeDesktopOS); //eslint-disable-line
