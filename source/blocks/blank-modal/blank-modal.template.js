export default (src) => {
  return `<section class="blank-modal"><h2 class="blank-modal__title"><span class="blank-modal__marked">Официальный</span> бланк</h2><div class="blank-modal__blank-image"><img src="${src}" alt="Официальный бланк результата"/></div></section>`;
};
