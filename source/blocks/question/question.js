import Validate from '../../js/common/validate';
import send from '../../js/common/send';
import Inputmask from "inputmask/dist/inputmask/inputmask.js";
import yaGoals from '../../js/common/ya-goals';

export default () => {
  const question = document.querySelector(`.question`);

  if (question !== null) {
    const telFields = question.querySelectorAll(`.field-text__input[type="tel"]`);

    if (telFields.length > 0) {
      const myInputMask = new Inputmask(`+7 999 999 99 99`, {showMaskOnHover: false});
      myInputMask.mask(telFields);
    }

    const form = question.querySelector(`.question__form`);
    const thanks = question.querySelector(`.question__thanks`);
    const thanksButton = question.querySelector(`.question__one-more`);

    const showThanks = () => {
      form.classList.add(`question__form--hidden`);
      thanks.classList.add(`question__thanks--visible`);
      thanksButton.addEventListener(`click`, onThanksButtonClicked);
    };

    const hideThanks = () => {
      form.classList.remove(`question__form--hidden`);
      thanks.classList.remove(`question__thanks--visible`);
      thanksButton.removeEventListener(`click`, onThanksButtonClicked);
    };

    const onThanksButtonClicked = (evt) => {
      evt.preventDefault();
      hideThanks();
    };

    const myValidate = new Validate(form);

    myValidate.init();

    form.addEventListener(`submit`, (evt) => {
      evt.preventDefault();
      if (myValidate.checkValidity().isValid) {
        yaGoals(`order15`);
        let data = new FormData(form);
        let template = `\
        <b>Имя: </b> {{name}} <br>
        <b>Электронный адрес: </b> {{email}} <br>
        <b>Текст обращения: </b> {{interest}} <br>
        <b>Телефон: </b> {{phone}}`;

        data.append(`template`, template);
        // data.append(`theme`, `New Request from Online School`);
        // data.append(`recipient`, `qa@donepro.ru`);
        // data.append(`sender`, `qa@donepro.ru`);
        // data.append(`replyTo`, `qa@donepro.ru`);

        send({
          url: form.action,
          method: form.method,
          data,
          onSuccess() {
            showThanks();
            form.reset();
          },
          onError() {
            showThanks();
          }
        });
      }
    });
  }
};
