module.exports = [{
  subject: `Английский язык`,
  title: `Как написать эссе на&nbsp;14&nbsp;баллов`,
  type: `article`,
  href: `kak-napisat-ege-na-14-ballov.html`
}, {
  subject: `Обществознание`,
  title: `План написания Эссе по&nbsp;Обществознанию`,
  type: `article`,
  href: `plan-napisaniya-esse-po-obshestvoznaniy.html`
}, {
  subject: `История`,
  title: `План сочинения по&nbsp;истории`,
  type: `article`,
  href: `plan-sochineniya-po-istorii.html`
}, {
  subject: `Русский язык`,
  title: `Структура сочинения ЕГЭ по&nbsp;русскому языку`,
  type: `article`,
  href: `structura-sochineniya-ege-po-russkomy-yaziky.html`
}];
