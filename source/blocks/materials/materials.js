const materialsList = document.querySelector(`.materials__list`);
const materialsButton = document.querySelector(`.materials__more`);

if (materialsButton !== null) {
  materialsButton.addEventListener(`click`, (evt) => {
    evt.preventDefault();
    materialsList.classList.add(`materials__list--all-visible`);
    materialsButton.classList.add(`materials__more--hidden`);
  });
}
