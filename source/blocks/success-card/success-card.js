export const getTemplate = (item) => {
  return `\
    <section class="success-card success-card--mobile">
      <div class="success-card__left">
        <div class="success-card__wrapper">
          <div class="success-card__social-link">
            <div class="${item.socialLink.image.mod ? `success-card__social-avatar success-card__social-avatar--` + item.socialLink.image.mod : `success-card__social-avatar`}">
              <img src="${item.socialLink.image.src}" width="${item.socialLink.image.width}" height="${item.socialLink.image.height}" alt="${item.socialLink.image.alt}">
            </div>
            ${item.socialLink.href !== null ? `<a class="success-card__social-type" href="${item.socialLink.href}" target="_blank">${item.socialType}</a>` : ``}
          </div>
          <h3 class="success-card__title">${item.name}</h3>
          <p class="success-card__success">${item.success}</p>
          <a class="blank-link" href="${item.link}" target="_blank" data-modal="blank" data-src="${item.link}"><span>Официальный бланк</span></a>
        </div>
      </div>
      <div class="success-card__right">
        <div class="success-card__statistics-list">
          <div class="success-card__statistics-item success-card__statistics-item--before">
            <div class="success-card__statistics-value">${item.statistics.before}
              <svg width="110" height="110" version="1.1" xmlns="http://www.w3.org/2000/svg" style="transform: rotate(-90deg);">
                <circle r="53" cx="55" cy="55" fill="transparent" stroke-width="2" stroke="#333" />
                <circle r="53" cx="55" cy="55" fill="transparent" stroke-dasharray="333" stroke-dashoffset="${item.statistics.before * (-3.328) + `px`}" stroke-width="3" stroke="#ebebeb"/>
              </svg>
            </div>
            <div class="success-card__statistics-name">1-й пробник</div>
          </div>
          <div class="success-card__statistics-item success-card__statistics-item--after">
            <div class="success-card__statistics-value">${item.statistics.after}
              <svg width="110" height="110" version="1.1" xmlns="http://www.w3.org/2000/svg" style="transform: rotate(-90deg);">
                <circle r="53" cx="55" cy="55" fill="transparent" stroke-width="2" stroke="#41e6a0"/>
                <circle r="53" cx="55" cy="55" fill="transparent" stroke-dasharray="333" stroke-dashoffset="${item.statistics.after * (-3.328) + `px`}" stroke-width="3" stroke="#ebebeb"/>
              </svg>
            </div>
            <div class="success-card__statistics-name">ЕГЭ</div>
          </div>
          <div class="success-card__statistics-item success-card__statistics-item--difference">
            <div class="success-card__statistics-value">${item.statistics.difference}
              <svg width="110" height="110" version="1.1" xmlns="http://www.w3.org/2000/svg" style="transform:rotate(${(item.statistics.before * 180 / 50) - 90}deg)">
                <circle r="53" cx="55" cy="55" fill="transparent" stroke-width="2" stroke="#f02d96"/>
                <circle r="53" cx="55" cy="55" fill="transparent" stroke-dasharray="333" stroke-dashoffset="${item.statistics.difference * (-3.328) + `px`}" stroke-width="3" stroke="#ebebeb"/>
              </svg>
            </div>
            <div class="success-card__statistics-name">Прогресс</div>
          </div>
          <div class="success-card__statistics-item success-card__statistics-item--university">
            <div class="success-card__statistics-value success-card__statistics-value--${item.statistics.university.mod}" title="${item.statistics.university.name}">${item.statistics.university.name}</div>
            <div class="success-card__statistics-name">${item.isGirl ? `Поступила` : `Поступил`}</div>
          </div>
        </div>
        <p class="success-card__course">${item.course.name} — <span class="success-card__course-points">${item.course.points}</span> ${item.course.text}</p>
      </div>
    </section>
  `;
};
