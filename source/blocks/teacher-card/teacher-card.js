document.addEventListener('click', function (evt) { //eslint-disable-line
  let activeElement = evt.target;
  if (activeElement.classList.contains('teacher-card__toggle-button')) { //eslint-disable-line
    activeElement.classList.add('teacher-card__toggle-button--hide'); //eslint-disable-line
    activeElement.previousSibling.classList.toggle('teacher-card__content--full-height'); //eslint-disable-line
  }
});
