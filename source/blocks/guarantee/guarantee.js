import slick from 'slick-carousel'; //eslint-disable-line
import $ from 'jquery';
const slider = document.querySelector(`.guarantee__list`);


function guaranteeWindowSize() {
  if ($(window).width() <= '767') { //eslint-disable-line
    $(slider).slick({
      accessibility: false,
      centerMode: true,
      appendDots: document.querySelector(`.guarantee__list`),
      arrows: false,
      dots: true,
      dotsClass: `guarantee__dots`,
      infinite: false,
      mobileFirst: true,
      rows: 0,
      variableWidth: true,
      swipeToSlide: true
    });
  }
}

$(window).on('load resize', guaranteeWindowSize); //eslint-disable-line
