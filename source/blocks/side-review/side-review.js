export const getTemplate = (item) => {
  return `\
<section class="side-review">
  <h3 class="side-review__title">${item.name}</h3>
  <a class="side-review__social-link" href="${item.socialLink.href}" target="_blank">
    <div class="side-review__social-avatar">
      <img src="${item.socialLink.image.src}" widht="${item.socialLink.image.width}" height="${item.socialLink.image.height}" alt="${item.socialLink.image.alt}">
    </div>
    <span class="side-review__social-type">${item.socialType}</span>
  </a>
  <p class="side-review__text">${item.text}</p>
  <ul class="side-review__results">
    ${item.results.map((result) => {
    return `\
      <li class="side-review__results-item">
        ${result.course} — <span class="side-review__results-points">${result.points}</span> ${result.text}
      </li>`;
  }).join(``)}
  </ul>
</section>`;
};
