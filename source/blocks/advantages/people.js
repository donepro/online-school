module.exports = [{
  socialLink: {
    href: null,
    image: {
      mod: `no-photo`,
      src: `img/avatar-abramchenko.jpg`,
      width: `110`,
      height: `110`,
      alt: `Аватар пользователя`
    }
  },
  socialType: `vk`,
  name: `Михаил Абрамченко`,
  isGirl: false,
  success: `Прошел <span class='success-card__marked-text'>Бронебойную подготовку</span> по&nbsp;всем предметам. Поступил в&nbsp;МГИМО на&nbsp;бюджет.`,
  link: `img/blank-abramchenko.jpg`,
  statistics: {
    before: `46`,
    after: `93`,
    difference: `47`,
    university: {
      name: `Московский государственный институт международных отношений (университет) Министерства иностранных дел Российской Федерации`,
      mod: `mgimo`
    }
  },
  course: {
    name: `Английский`,
    points: `93`,
    text: `балла`
  }
}, {
  socialLink: {
    href: `https://vk.com/id65293677`,
    image: {
      src: `img/avatar-kiroyanc.jpg`,
      width: `110`,
      height: `110`,
      alt: `Аватар пользователя Кироянц Ангелина`
    }
  },
  socialType: `vk`,
  name: `Кироянц Ангелина`,
  isGirl: true,
  success: `Прошла <span class='success-card__marked-text'>Бронебойную подготовку</span> по&nbsp;двум предметам (обществознание и&nbsp;история). Поступила в&nbsp;МГУ на&nbsp;бюджет.`,
  link: `img/blank-kiroyanc.png`,
  statistics: {
    before: `51`,
    after: `93`,
    difference: `42`,
    university: {
      name: `Московский государственный университет имени М. В. Ломоносова`,
      mod: `msu`
    }
  },
  course: {
    name: `История`,
    points: `93`,
    text: `балла`
  }
}, {
  socialLink: {
    href: `https://vk.com/love.love.natasha`,
    image: {
      src: `img/avatar-nikolenko.jpg`,
      width: `110`,
      height: `110`,
      alt: `Аватар пользователя Николенко Наталья`
    }
  },
  socialType: `vk`,
  name: `Николенко Наталья`,
  isGirl: true,
  success: `Прошла <span class='success-card__marked-text'>Бронебойную подготовку</span> по&nbsp;трем предметам (обществознание, русский и&nbsp;история). Поступила в&nbsp;НИУ РАНХиГС на&nbsp;бюджет.`,
  link: `img/blank-nikolenko.png`,
  statistics: {
    before: `53`,
    after: `93`,
    difference: `40`,
    university: {
      name: `Нижегородский институт управления — филиал Российской академии народного хозяйства и государственной службы при Президенте Российской Федерации`,
      mod: `ranepa`
    }
  },
  course: {
    name: `История`,
    points: `93`,
    text: `балла`
  }
}, {
  socialLink: {
    href: `https://vk.com/olga_afomix`,
    image: {
      src: `img/avatar-afanasieva.jpg`,
      width: `110`,
      height: `110`,
      alt: `Аватар пользователя Афанасьева Ольга`
    }
  },
  socialType: `vk`,
  name: `Афанасьева Ольга`,
  isGirl: true,
  success: `Прошла <span class='success-card__marked-text'>Бронебойную подготовку</span> по&nbsp;трем предметам (обществознание, русский и&nbsp;история). Поступила в&nbsp;МГУ на&nbsp;бюджет.`,
  link: `img/blank-afanasieva.jpg`,
  statistics: {
    before: `47`,
    after: `100`,
    difference: `53`,
    university: {
      name: `Московский государственный университет имени М. В. Ломоносова`,
      mod: `msu`
    }
  },
  course: {
    name: `Русский`,
    points: `100`,
    text: `баллов`
  }
}];
