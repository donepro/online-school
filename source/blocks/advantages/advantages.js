import slick from 'slick-carousel'; //eslint-disable-line
import $ from 'jquery';
const slider = document.querySelector(`.advantages__list`);

$(slider).slick({
  appendDots: document.querySelector(`.advantages__list-wrapper`),
  arrows: true,
  appendArrows: `.advantages__controls`,
  prevArrow: `<button type="button" class="advantages__arrow advantages__arrow--prev">Previous</button>`,
  nextArrow: `<button type="button" class="advantages__arrow advantages__arrow--next">Next</button>`,
  centerMode: true,
  dots: true,
  dotsClass: `advantages__dots`,
  infinite: false,
  initialSlide: 1,
  mobileFirst: true,
  rows: 0,
  variableWidth: true,
  focusOnSelect: true
});
