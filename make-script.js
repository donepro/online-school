const fs = require(`fs`);

const blockName = process.argv[2];
const PATH = `source/blocks`;
const dir = `${PATH}/${blockName}`;

fs.writeFileSync(`${dir}/index.js`, `import './${blockName}';\n`);
fs.writeFileSync(`${dir}/${blockName}.js`, ``);
