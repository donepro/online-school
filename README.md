# ЕГЭ

`npm start` - start development

`npm test` - linting js and scss

`npm run build` - development version

`npm run build:production` - production version

`npm run sort` - sort rules in scss files in the block folder

`npm run imagemin` - minify image in `source/img` folder

При разработке нужно обратить внимание на стайлгайд можно [тут](styleguide.md)